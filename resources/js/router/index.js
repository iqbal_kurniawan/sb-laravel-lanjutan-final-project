import ListRumahComponent from '../components/ListRumahComponent'
import DetailListRumahComponent from '../components/DetailListRumahComponent'
import ListPesananComponent from '../components/ListPesananComponent'
import DetailListPesananComponent from '../components/DetailListPesananComponent'

export default {
	mode:"history",
	linkActiveClass:"active",
	routes:[
		{
			path:"/home",
			name: "pages.listRumah",
			component:ListRumahComponent
		},
		{
			path:"/rumah/:rumah_id/detail",
			name:"rumah.detail",
			component:DetailListRumahComponent,
		},
		{
			path:"/lihatpesanan",
			name: "pages.listPesanan",
			component:ListPesananComponent
		},
		{
			path:"/pesanan/:booking_id/detail",
			name:"pesanan.detail",
			component:DetailListPesananComponent,
		},
	],
}