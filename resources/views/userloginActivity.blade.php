<!DOCTYPE html>
<html>
<head>
	<title>Log Activity Lists</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
</head>
<body>


<div class="container">
	<h1>User Login Log Activity Lists</h1>
	<table class="table table-bordered">
		<tr>
			<th>No</th>
			<th>Id User</th>
			<th>Use Name </th>
			<th>Subject</th>
			<th>Url</th>
			<th>Method</th>
			<th>Ip</th>
			<th>Created at</th>
		</tr>
		@if($logs->count())
			@foreach($logs as $key => $log)
			<tr>
				<td>{{ ++$key }}</td>
				<td>{{ $log->user_id }}</td>
				<td>{{ $log->user_name }}</td>
				<td>{{ $log->subject }}</td>
				<td class="text-success">{{ $log->url }}</td>
				<td><label class="label label-info">{{ $log->method }}</label></td>
				<td class="text-warning">{{ $log->ip }}</td>
				<td class="text-danger">{{ $log->created_at }}</td>
			</tr>
			@endforeach
		@endif
	</table>
</div>


</body>
</html>