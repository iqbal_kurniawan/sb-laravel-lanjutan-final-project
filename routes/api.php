
<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
return $request->user();

});


Route::group(['middleware' => 'auth:api'], function(){
// Untuk Authorization nya saya gunakan di template

	//Untuk Role Admin 
	Route::group(['middleware' => ['auth','role-admin']], function() {
		Route::get('rumah','RumahController@index');
		Route::get('/rumah/detail/{id}','RumahController@detail');
		Route::post('rumah','RumahController@store');
		Route::post('rumah/delete/{id}','RumahController@delete');
	});	

	Route::group(['middleware' => ['auth','role-member']], function() {
		Route::get('rumah','RumahController@index');
		Route::get('/rumah/detail/{id}','RumahController@detail');

		Route::post('/user/booking','BookingController@pesanKontrakan');
		Route::get('/user/booking/daftar/','BookingController@allpesanan');
		Route::get('/user/booking/detailPesanan/{id}','BookingController@lihatPesanan');
		Route::post('/user/booking/bayarPesanan/{id}','BookingController@bayarPesanan');

	});

	Route::get('/comment/{id}','CommentController@all_comments');
	Route::post('/comment','CommentController@store');
});

// Pembayaran
Route::post('/generate','MidtransController@generate');

