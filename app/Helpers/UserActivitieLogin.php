<?php


namespace App\Helpers;
use Request;
use App\LogActivity as LogActivityModel;
use App\UserActivitiesLogin;
use Illuminate\Support\Str; 

class UserActivitieLogin
{


    public static function addToLog($subject="")
    {
        $log = [];
        $log['id'] = Str::uuid();
        $log['user_id'] = auth()->check() ? auth()->user()->id : 1;
        $log['user_name'] = auth()->check() ? auth()->user()->name : '';
        $log['subject'] = $subject;
        $log['url'] = Request::fullUrl();
        $log['method'] = Request::method();
        $log['ip'] = Request::ip();
        UserActivitiesLogin::create($log);
    }


    public static function UserLogActivityLists()
    {
        return  UserActivitiesLogin::latest()->get();
    }


}