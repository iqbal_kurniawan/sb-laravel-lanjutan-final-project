<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RumahResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
         return [
            'rumah_id' => $this->id,
            'rumah' => $this->nama_rumah,
            'deskripsi' => $this->deskripsi,
            'fasilitas' => $this->fasilitas,
            'harga_sewa' => $this->harga_sewa
        ];
    }
}
