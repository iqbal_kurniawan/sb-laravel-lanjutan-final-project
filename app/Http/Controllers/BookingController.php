<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;	
use App\Booking;
use App\Rumah;
use App\Http\Resources\BookingResource;

class BookingController extends Controller
{
    public function pesanKontrakan(Request $request){
        \UserLoginActivitie::addToLog('User Pesan Kontrakan Kontrakan');
        
    	$pesan_kontrakan = Booking::create([
    		'id' => Str::uuid(),
    		'nama_rumah_id' => $request->nama_rumah_id,
    		'users_id' => $request->users_id,
    		'status_bayar' => 0,
    		'total_bayar' =>0,
    		'tanggal_bayar' => 0,
        ]);
        // return response()->json('status',200);
    	return new BookingResource($pesan_kontrakan);
    }

    public function bayarPesanan(Request $request, $id){
    	$updateDataBooking = Booking::find($id);
    	$updateDataBooking->status_bayar = 'Sudah Di Bayar';
    	$updateDataBooking->total_bayar = $request->total_bayar;
    	$updateDataBooking->tanggal_bayar = date("Y-m-d h:i:sa");
    	$updateDataBooking->save();
    	return $updateDataBooking;
    }

    public function allpesanan(){
        \UserLoginActivitie::addToLog('User Melihat Semua Pesanan');
        
        $getDataBooking = \DB::table('bookings')
                    ->select('bookings.id','rumahs.nama_rumah','rumahs.harga_sewa')->join('rumahs','bookings.nama_rumah_id','=','rumahs.id')->get();
        return new BookingResource($getDataBooking);
    }

    public function lihatPesanan($id){
        \UserLoginActivitie::addToLog('User Melihat Detail Pesanan ');
    	
        $getDataBooking = Booking::find($id);
    	$getDataRumah = Rumah::find($getDataBooking->nama_rumah_id);

    	if(($getDataBooking->status_bayar || $getDataBooking->total_bayar || $getDataBooking->tanggal_bayar) == null){
    		    $data_status_pembayaran = [
    				'status_bayar' => 'Belum Di Bayar',
    				'total_bayar' => '0',
    				'tanggal_bayar' => '--',
    			];
    	}else{
    		  $data_status_pembayaran = [
    				'status_bayar' => $getDataBooking->status_bayar,
    				'total_bayar' => $getDataBooking->total_bayar,
    				'tanggal_bayar' => $getDataBooking->tanggal_bayar,
    			];
    	}
	    	
    	$rows = [
    		'nama_rumah' =>$getDataRumah->nama_rumah,
    		'deskripsi'  => $getDataRumah->deskripsi,
    		'fasilitas'  => $getDataRumah->fasilitas,
    		'harga_sewa'  => $getDataRumah->harga_sewa,
    		'id_pemesanan' =>$getDataBooking->id,
    		'status_pembayaran' => $data_status_pembayaran,
    	];
    	return new BookingResource($rows);
    	


    }
}
