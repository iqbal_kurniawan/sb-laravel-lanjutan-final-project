<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Illuminate\Support\Str;	
use App\Events\ChatStoredEvent;

class CommentController extends Controller
{
    public function all_comments($id){
          \UserLoginActivitie::addToLog('User Melihat COmment');
    	$all_comments = \DB::table('comments')->select('comments.id','comments.comment','users.name','comments.created_at',
    					'rumahs.nama_rumah')
    					->join('users','comments.users_id','=','users.id')
    					->join('rumahs','comments.nama_rumah_id','=','rumahs.id')
    					->where('comments.nama_rumah_id',$id)
    					->orderBy('created_at','desc')
    					->get();

    	return $all_comments;
    }

    public function store(Request $request){
          \UserLoginActivitie::addToLog('User Mengirimkan COmment');
    	$add_comment = Comment::create([
    		'id' => Str::uuid(),
    		'comment'=>$request->comment,
    		'users_id' => $request->users_id,
    		'nama_rumah_id' => $request->nama_rumah_id,
    		'created_at' => $request->created_at,
    	]);

        broadcast(new ChatStoredEvent($add_comment))->toOthers();

    	return $add_comment;
    }

}
