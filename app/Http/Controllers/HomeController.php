<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        \UserLoginActivitie::addToLog('User Akses Halaman Home');
       
        return view('home');
    }

    public function userloginactivitie()
    {
        $logs = \UserLoginActivitie::UserLogActivityLists();
        return view('userloginActivity',compact('logs'));
      }

}
