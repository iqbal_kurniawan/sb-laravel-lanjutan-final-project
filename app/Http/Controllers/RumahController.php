<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rumah;	
use  App\Http\Resources\RumahResource;
use Illuminate\Support\Str;	

class RumahController extends Controller
{
    
	public function index(){
		$rumahs = Rumah::orderBy('created_at','desc')->get();
        \UserLoginActivitie::addToLog('User Melihat Daftar Kontrakan');
		return RumahResource::collection($rumahs);
    }

    public function store(Request $request){

        $add_rumah = Rumah::create([
    		'id' => Str::uuid(),
    		'nama_rumah' => $request->nama_rumah,
    		'deskripsi' => $request->deskripsi,
    		'fasilitas' => $request->fasilitas,
    		'harga_sewa' => $request->harga_sewa
    	]);
    	return new RumahResource($add_rumah);
    }

    public function detail($id){
        \UserLoginActivitie::addToLog('User Akses Halaman Detail Rumah');
        
        $detail_rumah = Rumah::find($id);
    	return new RumahResource($detail_rumah);
    }

    public function delete($id){
    	Rumah::destroy($id);
    	return 'data berhasil di hapus';
    }

}
