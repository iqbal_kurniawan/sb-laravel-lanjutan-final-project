<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	   protected $keyType = 'string';
       public $incrementing = false;
       protected $guarded = [];

       public function user(){
       		return $this->belongsTo(User::class);
       }
}
