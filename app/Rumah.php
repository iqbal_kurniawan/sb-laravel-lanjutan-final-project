<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rumah extends Model
{
        protected $keyType = 'string';
        public $incrementing = false;
        protected $guarded = [];
}
