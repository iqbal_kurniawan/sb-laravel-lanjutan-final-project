<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserActivitiesLogin extends Model
{
       protected $keyType = 'string';
       public $incrementing = false;
       // protected $guarded = [];
       protected $fillable = [
         'id','user_id', 'user_name','subject','url', 'method', 'ip'
    	];
}
